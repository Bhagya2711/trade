/**
 * 
 */
package com.demo.trading.exception;

/**
 * @author bhagyashree.naray
 *
 */
public class SellNotAllowedException extends Exception{

	
	
	private static final long serialVersionUID = 1L;

	public SellNotAllowedException(String message) {
		super(message);
	}
}
