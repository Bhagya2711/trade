/**
 * 
 */
package com.demo.trading.exception;

/**
 * @author bhagyashree.naray
 *
 */
public class BuyNotAllowedException extends Exception{

	
	
	private static final long serialVersionUID = 1L;

	public BuyNotAllowedException(String message) {
		super(message);
	}
}
