/**
 * 
 */
package com.demo.trading.service;

import com.demo.trading.exception.UserNotFoundException;
import com.demo.trading.request.LoginRequest;

/**
 * @author bhagyashree.naray
 *
 */
public interface LoginService {

	/**
	 * @param loginDto
	 * @return
	 * @throws UserNotFoundException 
	 */
	Object login(LoginRequest loginDto) throws UserNotFoundException;

}
