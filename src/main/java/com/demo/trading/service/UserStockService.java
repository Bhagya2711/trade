/**
 * 
 */
package com.demo.trading.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.demo.trading.dto.TransactionDetailsDto;
import com.demo.trading.exception.BuyNotAllowedException;
import com.demo.trading.exception.SellNotAllowedException;
import com.demo.trading.exception.UserNotFoundException;
import com.demo.trading.request.BuySellStockRequest;
import com.demo.trading.response.Response;

/**
 * @author bhagyashree.naray
 *
 */
@Service
public interface UserStockService {

	/**
	 * @param request
	 * @return
	 * @throws UserNotFoundException
	 * @throws SellNotAllowedException 
	 * @throws BuyNotAllowedException 
	 */
	Response buyOrSellStock(BuySellStockRequest request) throws UserNotFoundException, SellNotAllowedException, BuyNotAllowedException;

	public List<TransactionDetailsDto> findByUserIdAndTransactionType(Integer userId, String transactionType)
			throws UserNotFoundException;

}
