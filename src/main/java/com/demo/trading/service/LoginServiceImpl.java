/**
 * 
 */
package com.demo.trading.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.trading.controller.LoginController;
import com.demo.trading.entity.User;
import com.demo.trading.exception.UserNotFoundException;
import com.demo.trading.repository.UserRepository;
import com.demo.trading.request.LoginRequest;
import com.demo.trading.response.Response;

/**
 * @author bhagyashree.naray
 *
 */
@Service
public class LoginServiceImpl implements LoginService {

	/**
	 * 
	 */
	private static final String LOGIN_SUCCESSFUL = "Login successful";

	Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	UserRepository userRepository;

	@Override
	public Object login(LoginRequest loginRequest) throws UserNotFoundException {

		Optional<User> userOptional = userRepository.findByUserEmailAndPassword(loginRequest.getEmail(),
				loginRequest.getPassword());

		if (userOptional.isPresent()) {
			logger.info(LOGIN_SUCCESSFUL);
			return new Response(LOGIN_SUCCESSFUL, 200l);
		} else {
			logger.error("User not found");
			throw new UserNotFoundException("User not found");
		}

	}

}
