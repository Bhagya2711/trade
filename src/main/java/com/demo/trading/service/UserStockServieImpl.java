/**
 * 
 */
package com.demo.trading.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.trading.dto.QuantityDto;
import com.demo.trading.dto.TransactionDetailsDto;
import com.demo.trading.entity.StockTransaction;
import com.demo.trading.entity.User;
import com.demo.trading.enums.TransactionType;
import com.demo.trading.exception.BuyNotAllowedException;
import com.demo.trading.exception.SellNotAllowedException;
import com.demo.trading.exception.UserNotFoundException;
import com.demo.trading.feginclient.StockService;
import com.demo.trading.repository.StockTransactionRepository;
import com.demo.trading.repository.UserRepository;
import com.demo.trading.repository.UserStockRepository;
import com.demo.trading.request.BuySellStockRequest;
import com.demo.trading.response.Response;

/**
 * @author bhagyashree.naray
 *
 */
@Service
public class UserStockServieImpl implements UserStockService {

	/**
	 * 
	 */
	private static final String TRANSACTION_SUCCESSFUL = "Transaction Successful";

	/**
	 * 
	 */
	private static final String USER_NOT_FOUND = "User Not Found";

	private static final Logger logger = LoggerFactory.getLogger(UserStockServieImpl.class);

	@Autowired
	UserRepository userRepo;

	@Autowired
	StockTransactionRepository stockTransactionRepo;

	@Autowired
	UserStockRepository userStockRepository;
	
	@Autowired
	StockService stockService;

	
	@Override
	@Transactional
	public Response buyOrSellStock(BuySellStockRequest request) throws UserNotFoundException, SellNotAllowedException, BuyNotAllowedException {

		QuantityDto quantityDto=null;
		Optional<User> userOptional = userRepo.findById(request.getUserId());
		if (!userOptional.isPresent()) {
			logger.error(USER_NOT_FOUND);
			throw new UserNotFoundException(USER_NOT_FOUND);
		}
		
		StockTransaction dbRecord = stockTransactionRepo.findByUserAndStockId(userOptional.get(),request.getStockId());
		
		if (dbRecord != null) {
			if (!dbRecord.getTransactionDate().isBefore(LocalDateTime.now().minusHours(24))) {
				if (dbRecord.getTransactionType().equalsIgnoreCase(TransactionType.BUY.getTransaction())
						&& request.getTransactiontype().equalsIgnoreCase(TransactionType.SELL.getTransaction())) {

					throw new SellNotAllowedException("Cannot sell same stock which is bought within 24 hrs");

				}
				if (dbRecord.getTransactionType().equalsIgnoreCase(TransactionType.SELL.getTransaction())
						&& request.getTransactiontype().equalsIgnoreCase(TransactionType.BUY.getTransaction())) {

					throw new BuyNotAllowedException("Cannot buy same stock which is sold within 24 hrs");

				}
			}
		}
		
		
		/*Insert in transaction table*/
		logger.info("Inserting in transaction table");
		StockTransaction transaction = new StockTransaction();
		transaction.setStockId(Long.valueOf(request.getStockId()));
		transaction.setUser(userOptional.get());
		transaction.setTransactionType(request.getTransactiontype());
		transaction.setTransactionDate(LocalDateTime.now());
		transaction.setTransactionQuantity(request.getQuantity());
		
		 stockTransactionRepo.save(transaction);
		 
		/*Update quantity in stock service*/
		 logger.info("Update quantity in stock service");
		 if(request.getTransactiontype().equalsIgnoreCase(TransactionType.BUY.getTransaction())){
			 quantityDto  = new QuantityDto(TransactionType.BUY.getTransaction(), request.getQuantity(),request.getStockId());
			 
		 }else{
			 if(request.getTransactiontype().equalsIgnoreCase(TransactionType.SELL.getTransaction())){
				 quantityDto  = new QuantityDto(TransactionType.SELL.getTransaction(), request.getQuantity(),request.getStockId());
			 }
		 }
		 stockService.updateStock(quantityDto);
		
		return new Response(TRANSACTION_SUCCESSFUL,200l);
	}

	public List<TransactionDetailsDto> findByUserIdAndTransactionType(Integer userId, String transactionType)
			throws UserNotFoundException {

		if (!userRepo.findById(userId).isPresent()) {
			throw new UserNotFoundException(USER_NOT_FOUND);
		}
		List<StockTransaction> stockTransactionlist=null;
		logger.info("Fetching stock transaction details");
		if(transactionType==null) {
			 stockTransactionlist =userStockRepository.findByUserId(userId);
		}else {
		stockTransactionlist = userStockRepository.findByUserIdAndTransactionType(userId,
				transactionType);
		}
		List<TransactionDetailsDto> transactionDtoList = new ArrayList<>();
		stockTransactionlist.forEach(temp -> {
			TransactionDetailsDto detailsDto = new TransactionDetailsDto();
			detailsDto.setUserId(userId.longValue());
			BeanUtils.copyProperties(temp, detailsDto);
			transactionDtoList.add(detailsDto);
		});

		return transactionDtoList;
	}

	

}
