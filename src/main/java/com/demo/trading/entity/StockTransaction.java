/**
 * 
 */

package com.demo.trading.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bhagyashree.naray
 *
 */


@NoArgsConstructor
@Data
@AllArgsConstructor
@Entity
public class StockTransaction {

	@Column(name = "stock_id")
	Long stockId;

	@JoinColumn(name = "user_id")
	@ManyToOne
	User user;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "transaction_id")
	private Long transactionid;

	@Column(name = "transaction_type")
	private String transactionType;

	@Column(name = "transaction_date")
	private LocalDateTime transactionDate;

	@Column(name = "transaction_quantity")
	private Long transactionQuantity;

}
