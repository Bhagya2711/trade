package com.demo.trading.feginclient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.demo.trading.dto.QuantityDto;
import com.demo.trading.dto.QuantityResponse;
import com.demo.trading.dto.StockDto;


@FeignClient(name = "http://stock/stocks")
public interface StockService {

	@GetMapping
	public List<StockDto> getAllStocks();
	
	@PostMapping
	public QuantityResponse updateStock(@RequestBody QuantityDto quantityDto);
	
	
}
