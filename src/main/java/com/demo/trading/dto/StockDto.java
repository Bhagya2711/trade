package com.demo.trading.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StockDto {

	private Long stockId;
	private String stockName;
	private Double stockPrice;
	private Double difference;
	private String trend;
	private Long availableQuantity;


}
