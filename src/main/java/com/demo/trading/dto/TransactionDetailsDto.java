/**
 * 
 */
package com.demo.trading.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hemanth.garlapati
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDetailsDto {
	private Long userId;
	private Long stockId;
	private String transactionType;
	private LocalDateTime transactionDate;
	private Long transactionQuantity;

}
