package com.demo.trading.request;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * @author bhagyashree.naray
 * 
 */
@Data
@Component
public class LoginRequest {
	@NotNull(message="Email Id must not be null")
	private String email;
	
	@NotNull(message="Password must not be null")
	private String password;
	

}
