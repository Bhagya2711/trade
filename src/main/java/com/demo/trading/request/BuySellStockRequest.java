/**
 * 
 */
package com.demo.trading.request;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * @author bhagyashree.naray
 *
 */
@Data
@Component
public class BuySellStockRequest {
	@NotNull(message="User Id must not be null")
	Integer userId;
	
	@NotNull(message="Stock Id must not be null")
	Long stockId;
	
	//@Transactiontype
	String transactiontype;
	
	Long quantity;
	

}
