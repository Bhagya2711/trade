/**
 * 
 */
package com.demo.trading.enums;

/**
 * @author bhagyashree.naray
 *
 */
public enum ErrorCodes {
	USER_NOT_FOUND(4041l), LOGIN_FAILED(5001l),SELL_NOT_ALLOWED(4031l), BUY_NOT_ALLOWED(4032l);

	Long errorCode;

	ErrorCodes(Long value) {
		this.errorCode = value;
	}

	public Long getErrorCode() {
		return errorCode;
	}

}
