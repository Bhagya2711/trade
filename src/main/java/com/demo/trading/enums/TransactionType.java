/**
 * 
 */
package com.demo.trading.enums;

/**
 * @author bhagyashree.naray
 *
 */
public enum TransactionType {
	BUY("Buy"), SELL("Sell");

	private String transaction;

	TransactionType(String transaction) {
		this.transaction = transaction;
	}

	public String getTransaction() {
		return transaction;
	}
	
	

}
