/**
 * 
 */
package com.demo.trading.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.trading.entity.User;

/**
 * @author bhagyashree.naray
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	/**
	 * @param email
	 * @param password
	 * @return
	 */
	Optional<User> findByUserEmailAndPassword(String email, String password);

}
