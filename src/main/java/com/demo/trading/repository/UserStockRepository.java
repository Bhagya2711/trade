/**
 * 
 */
package com.demo.trading.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.trading.entity.StockTransaction;

/**
 * @author bhagyashree.naray
 *
 */
@Repository
public interface UserStockRepository extends JpaRepository<StockTransaction, Long> {

	@Query(value = "SELECT * FROM stock_transaction t WHERE t.transaction_date > DATE_SUB(CURDATE(), INTERVAL 1 DAY)"
			+ "and user_id=?1 and transaction_type=?2", nativeQuery = true)
	List<StockTransaction> findByUserIdAndTransactionType(Integer userId, String transactionType);

	@Query(value = "SELECT * FROM stock_transaction t WHERE t.transaction_date > DATE_SUB(CURDATE(), INTERVAL 1 DAY)"
			+ "and user_id=?1 ", nativeQuery = true)
	List<StockTransaction> findByUserId(Integer userId);

}
