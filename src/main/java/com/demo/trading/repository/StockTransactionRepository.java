/**
 * 
 */
package com.demo.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.trading.entity.StockTransaction;
import com.demo.trading.entity.User;

/**
 * @author bhagyashree.naray
 *
 */
public interface StockTransactionRepository extends JpaRepository<StockTransaction, Integer> {

	/**
	 * @param user
	 * @param stockId
	 */
	StockTransaction findByUserAndStockId(User user, Long stockId);

}
	