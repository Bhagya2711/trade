/**
 * 
 */
package com.demo.trading.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.trading.dto.StockDto;
import com.demo.trading.feginclient.StockService;

/**
 * @author bhagyashree.naray
 *
 */
@RestController
public class StocksController {
	@Autowired
	StockService stockService;

	
	/**Get all stocks
	 * @return List<StockDto>
	 */
	@GetMapping
	public ResponseEntity<List<StockDto>> getAllStocks() {
		return new ResponseEntity<>(stockService.getAllStocks(), HttpStatus.OK);

	}}
