/**
 * 
 */
package com.demo.trading.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.trading.exception.UserNotFoundException;
import com.demo.trading.request.LoginRequest;
import com.demo.trading.service.LoginService;

import io.swagger.annotations.ApiOperation;

/**
 * @author bhagyashree.naray
 *
 */
@RestController
public class LoginController {
	Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	LoginService loginService;
	

	/**Login
	 * @param loginDto
	 * @return
	 * @throws UserNotFoundException
	 */
	@ApiOperation(value = "User Login")
	@PostMapping("/login")
	public ResponseEntity<Object> login(@Valid @RequestBody  LoginRequest loginDto) throws UserNotFoundException {
		logger.info("LoginController: Inside login method");
		return new ResponseEntity<>(loginService.login(loginDto), HttpStatus.OK);
	}
	

}
