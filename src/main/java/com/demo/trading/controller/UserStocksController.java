/**
 * 
 */
package com.demo.trading.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.trading.dto.TransactionDetailsDto;
import com.demo.trading.exception.BuyNotAllowedException;
import com.demo.trading.exception.SellNotAllowedException;
import com.demo.trading.exception.UserNotFoundException;
import com.demo.trading.request.BuySellStockRequest;
import com.demo.trading.response.Response;
import com.demo.trading.service.UserStockService;

import io.swagger.annotations.ApiOperation;

/**
 * @author bhagyashree.naray
 *
 */
@RequestMapping
@RestController
public class UserStocksController {

	private static final Logger logger = LoggerFactory.getLogger(UserStocksController.class);

	@Autowired
	UserStockService userStockService;

	/**Buy or sell stock
	 * @param request
	 * @return Response
	 * @throws UserNotFoundException
	 * @throws SellNotAllowedException 
	 * @throws BuyNotAllowedException 
	 */
	@ApiOperation(value = "Buy or sell a stock")
	@PostMapping("/user-stocks")
	public ResponseEntity<Response> buyOrSellStock(@Valid @RequestBody BuySellStockRequest request)
			throws UserNotFoundException, SellNotAllowedException, BuyNotAllowedException {
		logger.info("UserStocksController: buyOrSellStock ");
		return new ResponseEntity<>(userStockService.buyOrSellStock(request), HttpStatus.OK);

	}

	/**
	 * 
	 * @param userId
	 * @param transactiontype
	 * @return list of TransactionDetailsDto
	 */

	@GetMapping(value = "/user-stocks")
	public ResponseEntity<List<TransactionDetailsDto>> getStockDetailsfromLast24hrs(
			@RequestParam(required = false) String transactiontype, @RequestParam Integer userId)
			throws UserNotFoundException {
		logger.info("Fetching details happened for last 24 hours for userId {} ", userId);
		return new ResponseEntity<>(userStockService.findByUserIdAndTransactionType(userId, transactiontype),
				HttpStatus.OK);
	}
}
