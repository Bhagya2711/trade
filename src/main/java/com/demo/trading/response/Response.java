/**
 * 
 */
package com.demo.trading.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author bhagyashree.naray
 *
 */
@Data
@AllArgsConstructor
public class Response {
	
	private String message;
	
	private Long statusCode;
	
	
	
	

}
