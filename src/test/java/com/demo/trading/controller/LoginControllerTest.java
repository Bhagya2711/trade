package com.demo.trading.controller;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.demo.trading.exception.UserNotFoundException;
import com.demo.trading.request.LoginRequest;
import com.demo.trading.response.Response;
import com.demo.trading.service.LoginServiceImpl;

@SpringBootTest
public class LoginControllerTest {

	@InjectMocks
	LoginController loginController;
	@Mock
	LoginServiceImpl loginServiceImpl;

	LoginRequest loginRequest = new LoginRequest();
	Response res;

	@BeforeEach
	public void init() {
		res = new Response("Login successful", 200l);
	}

	@Test
	public void loginMethod() throws UserNotFoundException {
		Mockito.when(loginServiceImpl.login(loginRequest)).thenReturn(res);
		ResponseEntity<Object> login = loginController.login(loginRequest);
		Assertions.assertEquals(HttpStatus.SC_OK, login.getStatusCodeValue());
	}

}
