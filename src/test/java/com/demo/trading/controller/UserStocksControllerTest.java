/**
 * 
 */
package com.demo.trading.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.trading.dto.TransactionDetailsDto;
import com.demo.trading.entity.StockTransaction;
import com.demo.trading.entity.User;
import com.demo.trading.exception.BuyNotAllowedException;
import com.demo.trading.exception.SellNotAllowedException;
import com.demo.trading.exception.UserNotFoundException;
import com.demo.trading.request.BuySellStockRequest;
import com.demo.trading.response.Response;
import com.demo.trading.service.UserStockService;
import com.demo.trading.service.UserStockServieImpl;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class UserStocksControllerTest {

	@Mock
	UserStockServieImpl userStockServieImpl;

	@InjectMocks
	UserStocksController userStockcontroller;
	
	@Mock
	UserStockService userStockService;

	@BeforeAll
	public static void setup() {

	}

	@Test
	public void testgetStockDetailsfromLast24hrs() throws UserNotFoundException {
		User user = new User();
		user.setUserId(1);
		user.setPassword("abc");
		user.setUserEmail("abc@gmail.com");
		TransactionDetailsDto dtoDetails = new TransactionDetailsDto();
		StockTransaction stockTransaction = new StockTransaction();
		stockTransaction.setStockId(1L);
		stockTransaction.setTransactionDate(LocalDateTime.now());
		stockTransaction.setUser(user);
		stockTransaction.setTransactionType("buy");
		stockTransaction.setTransactionQuantity(10L);
		BeanUtils.copyProperties(stockTransaction, dtoDetails);
		dtoDetails.setUserId(1L);
		List<TransactionDetailsDto> detailsDto = new ArrayList<>();
		detailsDto.add(dtoDetails);
		when(userStockServieImpl.findByUserIdAndTransactionType(1,"buy")).thenReturn(detailsDto);
		assertEquals(200, userStockcontroller.getStockDetailsfromLast24hrs("buy",1).getStatusCodeValue());

	}
	
	@Test
	public void testBuyOrSellStocks() throws UserNotFoundException, SellNotAllowedException, BuyNotAllowedException {
		BuySellStockRequest request = new BuySellStockRequest();
		request.setUserId(1);
		request.setStockId(3l);
		request.setTransactiontype("BUY");
		request.setQuantity(10l);
		
		Response response=new Response("Success", 200l);
		
		when(userStockServieImpl.buyOrSellStock(request)).thenReturn(response);
		assertEquals(200, userStockcontroller.buyOrSellStock(request).getStatusCodeValue());

	}

}
