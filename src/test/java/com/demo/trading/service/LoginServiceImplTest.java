package com.demo.trading.service;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.trading.entity.User;
import com.demo.trading.exception.UserNotFoundException;
import com.demo.trading.repository.UserRepository;
import com.demo.trading.request.LoginRequest;
import com.demo.trading.response.Response;
@SpringBootTest
public class LoginServiceImplTest {

	@InjectMocks
	LoginServiceImpl loginServiceImpl;
	
	@Mock
	UserRepository userRepository;
	LoginRequest loginRequest = new LoginRequest();
	
	User user=new User();
	@BeforeEach
	public void init() {
		
		List<User> list=new ArrayList<User>();
		loginRequest.setEmail("way2modi@gmail.com");
		loginRequest.setPassword("1234");
		
		user.setPassword("1234");
		user.setStockTransactions(null);
		user.setUserEmail("way2modi@gmail.com");
		user.setUserId(1);
		list.add(user);
	}
	
	
	@Test
	public void loginMethod() throws UserNotFoundException{
		
		
		Mockito.when(userRepository.findByUserEmailAndPassword(loginRequest.getEmail(), loginRequest.getPassword())).thenReturn(Optional.of(user));
		Response login = (Response) loginServiceImpl.login(loginRequest);
		Assertions.assertEquals(login.getStatusCode(),200l);
		
	}
	/*
	 * @Test public void loginMethodNegative() throws UserNotFoundException{
	 * loginRequest.setEmail("way2@gmail.com"); loginRequest.setPassword("1234");
	 * Mockito.when(userRepository.findByUserEmailAndPassword(loginRequest.getEmail(
	 * ), loginRequest.getPassword())).thenReturn(Optional.of(user));
	 * 
	 * assertThrows(UserNotFoundException.class, ()
	 * ->loginServiceImpl.login(loginRequest));
	 * 
	 * }
	 */
	
}
