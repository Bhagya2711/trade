/**
 * 
 */
package com.demo.trading.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.trading.dto.QuantityResponse;
import com.demo.trading.dto.TransactionDetailsDto;
import com.demo.trading.entity.StockTransaction;
import com.demo.trading.entity.User;
import com.demo.trading.exception.BuyNotAllowedException;
import com.demo.trading.exception.SellNotAllowedException;
import com.demo.trading.exception.UserNotFoundException;
import com.demo.trading.feginclient.StockService;
import com.demo.trading.repository.StockTransactionRepository;
import com.demo.trading.repository.UserRepository;
import com.demo.trading.repository.UserStockRepository;
import com.demo.trading.request.BuySellStockRequest;
import com.demo.trading.response.Response;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class UserStockServiceTest {

	@Mock
	UserStockRepository userStockRepository;
	
	@Mock
	UserRepository userRepository;

	@InjectMocks
	UserStockServieImpl userStockServieImpl;
	
	@Mock
	StockTransactionRepository stockTransactionRepo;
	
	@Mock
	StockService stockService;

	@BeforeAll
	public static void setup() {

	}

	@Test
	public void testFindByUserIdAndTransactionType() throws UserNotFoundException {

		User user = new User();
		user.setUserId(1);
		user.setPassword("abc");
		user.setUserEmail("abc@gmail.com");
		StockTransaction stockTransaction = new StockTransaction();
		TransactionDetailsDto dtoDetails = new TransactionDetailsDto();
		stockTransaction.setStockId(1L);
		stockTransaction.setTransactionDate(LocalDateTime.now());
		stockTransaction.setUser(user);
		stockTransaction.setTransactionType("buy");
		stockTransaction.setTransactionQuantity(10L);
		BeanUtils.copyProperties(stockTransaction, dtoDetails);
		dtoDetails.setUserId(1L);
		List<TransactionDetailsDto> detailsDto = new ArrayList<>();
		detailsDto.add(dtoDetails);
		List<StockTransaction> ls = new ArrayList<>();
		ls.add(stockTransaction);
		when(userRepository.findById(1)).thenReturn(Optional.of(user));
		when(userStockRepository.findByUserIdAndTransactionType(1, "buy")).thenReturn(ls);
		assertEquals(ls.size(), userStockServieImpl.findByUserIdAndTransactionType(1, "buy").size());
	}

	@Test
	public void testFindByUserIdAndTransactionTypeForException() {

		User user = new User();
		user.setUserId(2);
		user.setPassword("abc");
		user.setUserEmail("abc@gmail.com");
		StockTransaction stockTransaction = new StockTransaction();
		TransactionDetailsDto dtoDetails = new TransactionDetailsDto();
		stockTransaction.setStockId(1L);
		stockTransaction.setTransactionDate(LocalDateTime.now());
		stockTransaction.setUser(user);
		stockTransaction.setTransactionType("buy");
		stockTransaction.setTransactionQuantity(10L);
		BeanUtils.copyProperties(stockTransaction, dtoDetails);
		dtoDetails.setUserId(1L);
		List<TransactionDetailsDto> detailsDto = new ArrayList<>();
		detailsDto.add(dtoDetails);
		List<StockTransaction> ls = new ArrayList<>();
		ls.add(stockTransaction);
		when(userRepository.findById(2)).thenReturn(Optional.of(user));
		when(userStockRepository.findByUserIdAndTransactionType(1, "buy")).thenReturn(ls);
		assertThrows(UserNotFoundException.class, () -> userStockServieImpl.findByUserIdAndTransactionType(1, "buy"));
	}

	
	@Test
	public void testBuyStocks() {

		BuySellStockRequest request = new BuySellStockRequest();
		request.setUserId(55);
		request.setStockId(3l);
		request.setTransactiontype("BUY");
		request.setQuantity(10l);
		
		//when(userRepository.findById(2)).thenReturn(Optional.of(user));
		//when(userStockRepository.findByUserIdAndTransactionType(1, "buy")).thenReturn(ls);
		assertThrows(UserNotFoundException.class, () -> userStockServieImpl.buyOrSellStock(request));
	}

	@Test
	public void testBuyStocksSuccess() throws UserNotFoundException, SellNotAllowedException, BuyNotAllowedException {

		User user = new User();
		user.setUserId(2);
		user.setPassword("abc");
		user.setUserEmail("abc@gmail.com");
		
		StockTransaction transaction = new StockTransaction();
		transaction.setStockId(3l);
		transaction.setTransactionid(1l);transaction.setTransactionType("Buy");
		
		BuySellStockRequest request = new BuySellStockRequest();
		request.setUserId(1);
		request.setStockId(3l);
		request.setTransactiontype("BUY");
		request.setQuantity(10l);
		
		QuantityResponse resp = new QuantityResponse();
		resp.setMessage("Success");
		resp.setStatusCode(200);
				
		
		when(userRepository.findById(1)).thenReturn(Optional.of(user));
		when(userStockRepository.save(Mockito.any())).thenReturn(transaction);
		when(stockService.updateStock(Mockito.any())).thenReturn(resp);
		
		Response response=userStockServieImpl.buyOrSellStock(request);
		
		assertEquals("Transaction Successful", response.getMessage());
	}
}
